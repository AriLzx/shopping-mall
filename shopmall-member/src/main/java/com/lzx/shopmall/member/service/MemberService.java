package com.lzx.shopmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:19:18
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

