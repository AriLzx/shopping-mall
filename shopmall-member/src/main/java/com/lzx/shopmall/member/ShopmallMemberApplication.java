package com.lzx.shopmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 远程调用
 * 1、导入openFeign
 * 2、编写一个接口来进行远程调用
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = "com.lzx.shopmall.member.feign")
public class ShopmallMemberApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShopmallMemberApplication.class, args);
    }
}
