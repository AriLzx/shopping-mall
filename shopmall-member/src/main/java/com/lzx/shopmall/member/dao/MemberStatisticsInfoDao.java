package com.lzx.shopmall.member.dao;

import com.lzx.shopmall.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:19:17
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
