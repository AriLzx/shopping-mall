package com.lzx.shopmall.ware.dao;

import com.lzx.shopmall.ware.entity.WareOrderTaskDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:26:32
 */
@Mapper
public interface WareOrderTaskDetailDao extends BaseMapper<WareOrderTaskDetailEntity> {
	
}
