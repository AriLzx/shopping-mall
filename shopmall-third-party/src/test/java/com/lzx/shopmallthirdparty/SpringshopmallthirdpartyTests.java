package com.lzx.shopmallthirdparty;

import com.aliyun.oss.OSSClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringshopmallthirdpartyTests {
    @Autowired
    OSSClient ossClient;

    @Test
    public void testOSS() throws Exception {
        String bucketName = "shopmall-lzx";
        String objectName = "monkey.gif";
        String filePath = "D:\\DESTKOP\\monkey.gif";
        InputStream inputStream = new FileInputStream(filePath);
        ossClient.putObject(bucketName, objectName, inputStream);
        ossClient.shutdown();
        System.out.println("上传成功！");
    }
}
