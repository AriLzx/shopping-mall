package com.lzx.shopmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:22:19
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

