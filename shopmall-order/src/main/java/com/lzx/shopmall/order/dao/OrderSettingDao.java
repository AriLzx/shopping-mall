package com.lzx.shopmall.order.dao;

import com.lzx.shopmall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:22:19
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}
