package com.lzx.shopmall.product;

import com.lzx.shopmall.product.dao.CategoryBrandRelationDao;
import com.lzx.shopmall.product.entity.BrandEntity;
import com.lzx.shopmall.product.entity.CategoryBrandRelationEntity;
import com.lzx.shopmall.product.service.BrandService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShopmallProductApplicationTests {
    @Autowired
    CategoryBrandRelationDao categoryBrandRelationDao;

    @Test
    public void contextLoads() {
        categoryBrandRelationDao.updateCategory(255L, "手机11111");
        System.out.println("修改成功！");

    }
}
