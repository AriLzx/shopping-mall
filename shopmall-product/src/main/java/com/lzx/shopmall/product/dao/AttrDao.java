package com.lzx.shopmall.product.dao;

import com.lzx.shopmall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:52
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
