package com.lzx.shopmall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 整合mybatis-plus
 */
@EnableFeignClients(basePackages = "com.lzx.shopmall.product.feign")
@EnableDiscoveryClient
@MapperScan("com.lzx.shopmall.product.dao")
@SpringBootApplication
public class ShopmallProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShopmallProductApplication.class, args);
    }
}
