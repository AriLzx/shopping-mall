package com.lzx.shopmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.product.entity.SpuInfoEntity;
import com.lzx.shopmall.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:52
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo vo);

    void saveBaseSpuInfo(SpuInfoEntity infoEntity);

    PageUtils queryPageByCondition(Map<String, Object> params);
}

