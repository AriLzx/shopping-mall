package com.lzx.shopmall.product.dao;

import com.lzx.shopmall.product.entity.SkuSaleAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku销售属性&值
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:53
 */
@Mapper
public interface SkuSaleAttrValueDao extends BaseMapper<SkuSaleAttrValueEntity> {
	
}
