package com.lzx.shopmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.product.entity.SkuInfoEntity;

import java.util.Map;

/**
 * sku信息
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:53
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuInfo(SkuInfoEntity skuInfoEntity);

    PageUtils queryPageByCondition(Map<String, Object> params);
}

