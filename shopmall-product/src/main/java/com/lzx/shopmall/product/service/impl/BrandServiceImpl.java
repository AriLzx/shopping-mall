package com.lzx.shopmall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.common.utils.PageUtils;
import com.lzx.common.utils.Query;
import com.lzx.shopmall.product.dao.BrandDao;
import com.lzx.shopmall.product.entity.BrandEntity;
import com.lzx.shopmall.product.service.BrandService;
import com.lzx.shopmall.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //获取到key
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> warpper = new QueryWrapper<BrandEntity>();

        if (!StringUtils.isEmpty(key)) {
            warpper.eq("brand_id", key).or().like("name", key);
        }
        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                warpper
        );

        return new PageUtils(page);
    }

    @Override
    public void updateDetils(BrandEntity brand) {
        //保证冗余字段一致
        this.updateById(brand);
        if(!StringUtils.isEmpty(brand.getName())){
            //同步更新品牌名称
            categoryBrandRelationService.updateBrand(brand.getBrandId(),brand.getName());
            //TODO 更新其他关联
        }
    }

}