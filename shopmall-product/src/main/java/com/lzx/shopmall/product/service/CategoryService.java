package com.lzx.shopmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:53
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();
    //删除菜单
    void removeMenusByIds(List<Long> asList);
    //找到cateLogId的完整路径【父->子->孙】
    Long[] findCateLogPath(Long cateLogId);

    void updateCascade(CategoryEntity category);
}

