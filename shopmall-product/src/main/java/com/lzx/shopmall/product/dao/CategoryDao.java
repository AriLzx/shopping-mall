package com.lzx.shopmall.product.dao;

import com.lzx.shopmall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:53
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
