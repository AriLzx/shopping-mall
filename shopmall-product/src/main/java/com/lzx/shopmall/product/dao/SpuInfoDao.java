package com.lzx.shopmall.product.dao;

import com.lzx.shopmall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 20:49:52
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
