package com.lzx.shopmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.coupon.entity.HomeSubjectSpuEntity;

import java.util.Map;

/**
 * 专题商品
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:11:40
 */
public interface HomeSubjectSpuService extends IService<HomeSubjectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

