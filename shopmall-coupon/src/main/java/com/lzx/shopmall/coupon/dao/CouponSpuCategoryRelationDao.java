package com.lzx.shopmall.coupon.dao;

import com.lzx.shopmall.coupon.entity.CouponSpuCategoryRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券分类关联
 * 
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:11:40
 */
@Mapper
public interface CouponSpuCategoryRelationDao extends BaseMapper<CouponSpuCategoryRelationEntity> {
	
}
