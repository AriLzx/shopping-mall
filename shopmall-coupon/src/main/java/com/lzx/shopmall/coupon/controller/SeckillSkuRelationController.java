package com.lzx.shopmall.coupon.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lzx.shopmall.coupon.entity.SeckillSkuRelationEntity;
import com.lzx.shopmall.coupon.service.SeckillSkuRelationService;
import com.lzx.common.utils.PageUtils;
import com.lzx.common.utils.R;



/**
 * 秒杀活动商品关联
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:11:40
 */
@RestController
@RequestMapping("coupon/smsseckillskurelation")
public class SeckillSkuRelationController {
    @Autowired
    private SeckillSkuRelationService seckillSkuRelationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("coupon:smsseckillskurelation:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = seckillSkuRelationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("coupon:smsseckillskurelation:info")
    public R info(@PathVariable("id") Long id){
		SeckillSkuRelationEntity smsSeckillSkuRelation = seckillSkuRelationService.getById(id);

        return R.ok().put("smsSeckillSkuRelation", smsSeckillSkuRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("coupon:smsseckillskurelation:save")
    public R save(@RequestBody SeckillSkuRelationEntity smsSeckillSkuRelation){
		seckillSkuRelationService.save(smsSeckillSkuRelation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("coupon:smsseckillskurelation:update")
    public R update(@RequestBody SeckillSkuRelationEntity smsSeckillSkuRelation){
		seckillSkuRelationService.updateById(smsSeckillSkuRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("coupon:smsseckillskurelation:delete")
    public R delete(@RequestBody Long[] ids){
		seckillSkuRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
