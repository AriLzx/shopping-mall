package com.lzx.shopmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.common.utils.PageUtils;
import com.lzx.shopmall.coupon.entity.CouponSpuCategoryRelationEntity;

import java.util.Map;

/**
 * 优惠券分类关联
 *
 * @author liuzexuan
 * @email liuzx206@163.com
 * @date 2023-01-08 22:11:40
 */
public interface CouponSpuCategoryRelationService extends IService<CouponSpuCategoryRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

