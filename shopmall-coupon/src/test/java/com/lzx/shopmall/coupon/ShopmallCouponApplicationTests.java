package com.lzx.shopmall.coupon;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.function.*;
import java.util.stream.Collectors;

@SpringBootTest
public class ShopmallCouponApplicationTests {
    List<StudentTest> student = Arrays.asList(
            new StudentTest("刘泽轩", 23, 320),
            new StudentTest("傲傍傍", 21, 300),
            new StudentTest("傍傲", 20, 299),
            new StudentTest("菜鸡", 19, 360),
            new StudentTest("老肥", 8, 289)
    );

    @Test
    public void contextLoads() {
        int num = 1;
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello World" + num);
            }
        };
        r.run();
        Runnable r1 = () -> System.out.println("Hello Lambda" + num);
        r1.run();
    }

    @Test
    public void Test1() {
        Consumer<String> con = System.out::println;
        con.accept("傲傍头儿可爱在顶尖!");
    }

    @Test
    public void Test2() {
        Comparator<Integer> com = Integer::compare;
        Integer compare = com.compare(1, 3);
        System.out.println(compare);
    }

    /**
     * 消费类接口
     * Consumer<T>
     */
    @Test
    public void Test3() {
        grade(330, x -> System.out.println("刘泽轩考研考了" + x + "分成功上岸"));
    }

    public void grade(Integer scoer, Consumer<Integer> con) {
        con.accept(scoer);
    }

    /**
     * 供给性接口
     * Supplier<T>
     */
    @Test
    public void Test4() {
        List<Integer> numberList = getNumberList(10, () -> (int) (Math.random() * 100));
        for (Integer num : numberList) {
            System.out.println(num);
        }
    }

    public List<Integer> getNumberList(Integer num, Supplier<Integer> sup) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            Integer n = sup.get();
            list.add(n);
        }
        return list;
    }

    /**
     * Function<T,R>函数式接口
     */
    @Test
    public void Test5() {
        String s = stringHandler("\t\t\t刘泽轩考上了研究生!", String::trim);
        System.out.println(s);
        String s1 = stringHandler("\t\t\t刘泽轩考上了研究生!", S -> S.substring(0, 13)).trim();
        System.out.println(s1);
        System.out.println("上岸了");
    }

    public String stringHandler(String str, Function<String, String> fun) {
        return fun.apply(str);
    }

    /**
     * Predicate<T> 断言式接口
     */
    @Test
    public void Test6() {
        List<String> stringList = Arrays.asList("aobang", "laofei", "caiji", "maofei", "caiao", "laolao", "maocai");
        List<String> newList = fliterList(stringList, e -> e.length() > 5);
        for (String str : newList) {
            System.out.println(str);
        }
    }

    public List<String> fliterList(List<String> list, Predicate<String> pre) {
        List<String> strList = new ArrayList<>();
        for (String str : list) {
            if (pre.test(str)) {
                strList.add(str);
            }
        }
        return strList;
    }

    /**
     * 方法引用
     * 对象::实例方法名
     * 类::静态方法名
     * 类::实例方法名
     */
    @Test
    public void Test7() {
        BiPredicate<String, String> bp = String::equals;
        boolean test = bp.test("aobang", "aobang");
        System.out.println(test);
    }

    /**
     * Stream流的使用
     */
    @Test
    public void Test8() {
        student.stream().filter(s -> s.getScore() > 300).map(StudentTest::getName).forEach(System.out::println);
    }

    /**
     * 收集collector
     * 把所有的名字汇总起来放一个集合
     */
    @Test
    public void Test9() {
        List<String> list = student.stream().map(StudentTest::getName).collect(Collectors.toList());
        list.forEach(System.out::println);
    }

    /**
     * FrokJoin框架
     */
    @Test
    public void Test10() {
        Instant start = Instant.now();
        ForkJoinPool pool = new ForkJoinPool();
        ForkJoinTask<Long> task = new ForkJoinCalculate(0, 100000000000L);
        Long sum = pool.invoke(task);
        Instant end = Instant.now();
        System.out.println("响应时间为:" + Duration.between(start, end).toMillis());//10747
    }

    /**
     * 普通的线程遍历
     */
    @Test
    public void Test11() {
        Instant start = Instant.now();
        long sum = 0L;
        for (long i = 0; i < 100000000000L; i++) {
            sum += i;
        }
        System.out.println(sum);
        Instant end = Instant.now();
        System.out.println("响应时间为:" + Duration.between(start, end).toMillis());//17282
    }
}
